"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    
    Table driven approach: this is based off of the tax income ex from
    https://www.codeproject.com/Articles/42732/Table-driven-Approach

    NOTE: this will not validate that the values being passed in are float or int. 
    That is handled with flask_brevets.py
    """
    #var used to track the table, the amount of time and the table itself
    time = 0.0
    maxx=0
    rate=1
    prev_trigger=0
    race_distance = [(200 , 34), (400, 32), (600, 30), (1000, 28), (1300, 26)]
    # check if the control is larger then the brevet. If so, make control=brevet since the times 
    #past the brevet are equal at the brevet length.
    if (brevet_dist_km <= control_dist_km):
    	control_dist_km=brevet_dist_km
    #Here is the table. This subtracts previous distances when calculating time in order to apply 
    #the time based on speed for the specific range. So 0-200km is 200/34. and 0-400km is 200/34 + 200/32.	
    for trigger in race_distance:
    	time += (min(trigger[maxx], control_dist_km) - prev_trigger)/trigger[rate]  
    	if control_dist_km <= trigger[0]:
    		break
    	prev_trigger = trigger[maxx]
    #Set the arrow from argument to the arrow we are going to use. Calculate time in hrs and mins. If minutes round up, subtract 60 from min and add 1 to hrs.	
    arw = arrow.get(brevet_start_time)
    hrs = int(time)
    minu= round((time-hrs) *60)
    if minu ==60:
    	hrs += 1
    	minu -=60
    #Take the calculated time and shift the arrow value.
    arw=arw.shift(hours=+hrs, minutes=+minu)				
    return arw.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.

    Table driven approach: this is based off of the tax income ex from
    https://www.codeproject.com/Articles/42732/Table-driven-Approach. The table is shorter 
    here since min speed from 0-600km is the same (15)

    NOTE: this will not validate that the values being passed in are float or int. 
    That is handled with flask_brevets.py
    """
    #var used to track the table, the amount of time and the table itself    
    time = 0.0
    maxx=0
    rate=1
    prev_trigger=0
    race_distance = [(600, 15), (1000, 11.428), (1300, 13.33)] 

    # check if the control is larger then the brevet. If so, make control=brevet since the times 
    #past the brevet are equal at the brevet length.
    if (brevet_dist_km <= control_dist_km):
    	control_dist_km=brevet_dist_km
    for trigger in race_distance:
    	time += (min(trigger[maxx], control_dist_km) - prev_trigger)/trigger[rate]  
    	if control_dist_km <= trigger[0]:
    		break
    	prev_trigger = trigger[maxx]
    #make sure we account for start time having an hr time frame. This is an ambuguity based on description in acp times.
    #this will add an hr to the time if less then an hr. It is not suggested that you place a brevet within the first 15KM
    if time <=1:
    	time +=1
    
    #Set the arrow from argument to the arrow we are going to use. Calculate time in hrs and mins. If minutes round up, subtract 60 from min and add 1 to hrs.
    #There was also a weird issue calculating 200KM. As stated in the decription, it should be 13H30, but there is an exception for it to be at 13H20.	
    arw = arrow.get(brevet_start_time)
    hrs = int(time)
    minu= round((time-hrs) *60)

    if brevet_dist_km == 200:
    	minu +=10

    if minu >=60:
        hrs += 1
        minu -= 60

    arw=arw.shift(hours=+hrs, minutes=+minu)				
    return arw.isoformat()