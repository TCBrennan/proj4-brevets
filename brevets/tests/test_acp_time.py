from acp_times import open_time, close_time

import arrow
import nose
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)
arw = arrow.get("2020-05-30T08:00:00-08:00")

'''
Testing the folling cases. Error handling for out of range numbers is handled 
in flask_brevets and calc.html

1000km ACP BREVET
Checkpoint       Date  Time
==========       ====  ====
    0km   start: 30/05 08:00
  (0mi)   close: 30/05 09:00

  200km    open: 30/05 13:53
(124mi)   close: 30/05 21:20

  400km    open: 30/05 20:08
(249mi)   close: 31/05 10:40

  600km    open: 31/05 02:48
(373mi)   close: 01/06 00:00

 1000km    open: 31/05 17:05
(621mi)   close: 02/06 11:00

 1200km    open: 31/05 17:05
(746mi)   close: 02/06 11:00
'''
def same(s, t):
    """
    Same characters (possibly in different order)
    in two strings s and t.
    """
    return sorted(s) == sorted(t)

def test_open_time_corners():
	
	assert open_time(200, 1000, arw.isoformat()) == "2020-05-30T13:53:00-08:00"
	assert open_time(1000, 1000, arw.isoformat()) == "2020-05-31T17:05:00-08:00"

def test_close_time_corners():

	assert close_time(0, 1000, arw.isoformat()) == "2020-05-30T09:00:00-08:00"
	assert close_time(1000, 1000, arw.isoformat()) == "2020-06-02T11:00:00-08:00"

def test_open_past_bounds():
	
	assert same(open_time(200, 200, arw.isoformat()), open_time(240, 200, arw.isoformat()))
	assert same(open_time(400, 400, arw.isoformat()), open_time(480, 400, arw.isoformat()))
	assert same(open_time(1000, 1000, arw.isoformat()), open_time(1200, 1000, arw.isoformat()))

def test_close_past_bounds():
	
	assert same(close_time(200, 200, arw.isoformat()), close_time(240, 200, arw.isoformat()))
	assert same(close_time(400, 400, arw.isoformat()), close_time(480, 400, arw.isoformat()))
	assert same(close_time(600, 600, arw.isoformat()), close_time(720, 600, arw.isoformat()))
	assert same(close_time(1000, 1000, arw.isoformat()), close_time(1200, 1000, arw.isoformat()))
	
def test_random():
	assert open_time(600, 1000, arw.isoformat()) == "2020-05-31T02:48:00-08:00"
	assert close_time(400, 1000, arw.isoformat()) == "2020-05-31T10:40:00-08:00"
	assert same(open_time(600, 600, arw.isoformat()), open_time(720, 600, arw.isoformat()))
