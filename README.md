# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

## Author: Taylor C. Brennan, tbrennan@uoregon.edu ##

## Purpose ## 

This is designed to calculate the controls times for a brevet race. This has a running server and proccess JSON requests. By adding the control distance for a brevet, it will give the open and close times. Those entered distances are validated by checking if it is within the allowed range for the brevet and that only a float was passed in. It will calculate the results and return the control times where the clients page updates the fields with the times. 

## ACP controle times

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). 
As noticed from the calculator, 
-any control past the brevet distance is equal to the brevent distance.
-can only have controls up to 120% of brevet (ie. a 200km brevet can have a max of 240KM) Validation for this is handled though flask_brevets.py
-can only have floats.  Validation for this is handled though flask_brevets.py
-closing times before 15KM will add an hr (count for open time) like the calculator, it it not suggested to have controls so close to the start
-in both open and close, time are calculated based on the distance range. (0-200) (200-400) etc. So for 400KM, the first 200kM will be calculated by the rules of 0-200 and the second 200km will be calculated with the rules of 200-400.

We are essentially replacing the calculator here (https://rusa.org/octime_acp.html). We can also use that calculator to clarify requirements and develop test data.  

## AJAX and Flask reimplementation

The RUSA controle time calculator is a Perl script that takes an HTML form and emits a text page in the above link. 

The implementation that you will do will fill in times as the input fields are filled using Ajax and Flask. Currently the miles to km (and vice versa) is implemented with Ajax. You'll extend that functionality as follows:

* Each time a distance is filled in, the corresponding open and close times should be filled in with Ajax.   

* If out of range or bad characted are used, the open and close field will be blank, and there will be an error message at the bottom of the table

## Testing

There is a test_acp_time.py that runs the current test. Nose testing for the JSON responce is currently being worked on.

In the testing, there is a copied control distance from the provided sites to validate.





## Tasks

The code under "brevets" can serve as a starting point. It illustrates a very simple Ajax transaction between the Flask server and javascript on the web page. At present the server does not calculate times. It just returns double the number of miles. Other things may be missing; add them as needed. As before, you should fork and then clone the bitbucket repository, make your changes, and turn in the URL of your repository.

You'll turn in your credentials.ini using which we will get the following:

* The working application.

* A README.md file that includes not only identifying information (your name, email, etc.) but but also a revised, clear specification of the brevet controle time calculation rules.

* An automated 'nose' test suite.

* Dockerfile

## Grading Rubric

* If your code works as expected: 100 points. This includes:
	* AJAX in the frontend. That is, open and close times are automatically populated, 
	* Logic in the backend (acp_times.py), 
	* Frontend to backend interaction (with correct requests/responses), 
	* README is updated with a clear specification, and 
	* All five tests pass.

* If the AJAX logic is not working, 10 points will be docked off. 

* If the README is not clear or missing, up to 15 points will be docked off. 

* If the test cases fail, up to 15 points will be docked off. 

* If the logic in the acp_times.py file is wrong or is missing in the appropriate location, 30 points will be docked off.

* If none of the functionalities work, 30 points will be given assuming 
    * The credentials.ini is submitted with the correct URL of your repo, and
    * The Dockerfile builds without any errors
    
* If the Dockerfile doesn't build or is missing, 10 points will be docked off.

* If credentials.ini is missing, 0 will be assigned.